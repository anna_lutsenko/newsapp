//
//  TopNewsViewController.swift
//  NewsApp
//
//  Created by Anna on 01.10.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import UIKit

class TopNewsViewController: UIViewController {
    
    @IBOutlet weak var imageNews: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelLink: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var viewGradient: UIView!
    
    let gradientLayer = CAGradientLayer()
    var pieceOfNews: NewsModel? = nil
    var pageIndex: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        downloadData()
        createGradient()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        updateGradientLayerFrame()
    }
    
    @IBAction func openNews(_ sender: Any) {
        guard let newsURLString = pieceOfNews?.link,
            let url = URL(string: newsURLString) else {
            return
        }
        
        UIApplication.shared.open(url, options: [ : ], completionHandler: nil)
    }
    
    static func instanceVC() -> TopNewsViewController? {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TopNewsViewController") as? TopNewsViewController
        return vc
    }
    
    func downloadData() {
        guard let news = pieceOfNews else { return }
        labelTitle.text = news.title
        labelLink.text = news.domainName
        labelDate.text = news.date
        if let url = news.coverURL {
            imageNews.kf.setImage(with: url)
        }
    }
    
    func updateGradientLayerFrame() {
        var frame = viewGradient.bounds
        frame.size.width = UIScreen.main.bounds.width
        gradientLayer.frame = frame
    }
    
    func createGradient() {
        let color1 = UIColor.clear.cgColor
        let color2 = UIColor(white: 0, alpha: 0.85).cgColor
        
        gradientLayer.colors = [color1, color2]
        gradientLayer.locations = [0, 1]
        viewGradient.layer.insertSublayer(gradientLayer, at: 0)
    }
}
