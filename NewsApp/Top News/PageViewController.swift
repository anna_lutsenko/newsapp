//
//  PageViewController.swift
//  NewsApp
//
//  Created by Anna on 01.10.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import UIKit

class PageViewController: UIPageViewController {
    var topView: UIView?
    var pageControl = UIPageControl()
    var pages : [TopNewsViewController] = []
    var index : Int?
    var topNews: [NewsModel] = [] {
        didSet {
            setupFirstVC()
            pageControl.numberOfPages = topNews.count
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        delegate = self
        
        configurePageControl()
        configureTopView()
        setupFirstVC()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        topView?.frame = CGRect(x: self.view.frame.width - 95, y: 30, width: 110, height: 40)
        topView?.layer.cornerRadius = 20
        pageControl.frame = CGRect(x: 0,y: self.view.frame.height - 50, width: UIScreen.main.bounds.width,height: 50)
        
    }
    
    func setupFirstVC() {
        guard let firstVC = getViewControllerAtIndex(0) else {return}
        self.setViewControllers([firstVC], direction: .forward, animated: false, completion: nil)
    }
    
    private func configurePageControl() {
        // The total number of pages that are available is based on how many available colors we have.
        
        pageControl = UIPageControl(frame: CGRect(x: 0,y: self.view.frame.height - 50,width: UIScreen.main.bounds.width,height: 50))
        self.pageControl.numberOfPages = topNews.count
        self.pageControl.currentPage = 0
        self.pageControl.tintColor = UIColor.black
        self.pageControl.pageIndicatorTintColor = UIColor.white
        self.pageControl.currentPageIndicatorTintColor = UIColor(r: 21, g: 174, b: 239)
        self.view.addSubview(pageControl)
 
    }
    
    func configureTopView() {
        guard let topView = Bundle.main.loadNibNamed("TopView", owner: self, options: nil)?.first as? UIView else {  return  }
        self.topView = topView
        self.view.addSubview(topView)
    }
    
}

extension PageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        let newsVC: TopNewsViewController = viewController as! TopNewsViewController
        var index = newsVC.pageIndex
        
        if index == 0 {
            index = topNews.count - 1
        } else if index == NSNotFound {
            return nil
        } else {
            index -= 1
        }
        return getViewControllerAtIndex(index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let newsVC: TopNewsViewController = viewController as! TopNewsViewController
        
        var index = newsVC.pageIndex
        
        if index == NSNotFound {
            return nil
        }
        index += 1
        if index >= topNews.count {
            index = 0
        }
        return getViewControllerAtIndex(index)
    }
    
    
    
    func getViewControllerAtIndex(_ index: Int) -> TopNewsViewController? {
        guard index < topNews.count else { return nil }
        
        let topNewsVC = TopNewsViewController.instanceVC()
        topNewsVC?.pieceOfNews = topNews[index]
        topNewsVC?.pageIndex = index
        return topNewsVC
    }
}

extension PageViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard let currentIndex = (viewControllers?.first as? TopNewsViewController)?.pageIndex  else { return }
        pageControl.currentPage = currentIndex
    }
}
