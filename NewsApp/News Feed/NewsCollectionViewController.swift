//
//  NewsCollectionViewController.swift
//  NewsApp
//
//  Created by Anna on 30.09.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import UIKit
import Kingfisher
import SVProgressHUD

private let reuseIdentifier = "reuseIdentifier"
private let heightCell : CGFloat = 310
private let widthScreen = UIScreen.main.bounds.width

class NewsCollectionViewController: UICollectionViewController {
    
    /// UI Elements
    let myRefreshControl = UIRefreshControl()
    
    /// Managers
    let requestManager = RequestManager.sharedInstance
    
    /// Models
    var news : [NewsModel] = []
    
    let pageVC = PageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initController()
    }
    
    func initController() {
        downloadData()
        createItemLayout()
        //
        SVProgressHUD.setDefaultStyle(.dark)
        SVProgressHUD.setMaximumDismissTimeInterval(2)
        //
        initRefreshControl()
    }
    
    func initRefreshControl() {
        myRefreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        collectionView?.addSubview(myRefreshControl)
    }
    
    @objc func refresh() {
        downloadData()
    }
    
    func downloadData() {
        SVProgressHUD.show()
        requestManager.getData(success: { news in
            self.news = news
            DispatchQueue.main.async {
                SVProgressHUD.showSuccess(withStatus: nil)
                self.collectionView?.reloadData()
                /*              It's right approach but when I was working with API I noticed that there was no any top news.
                 so I decided to show random count of news to show you how PageViewController works.
                 
                 let topNews = news.filter{$0.isTopNews}
                 if !topNews.isEmpty {
                 self.addTopNews()
                 self.pageVC.topNews = topNews
                 }
                 */
                self.addTopNews()
                if news.count >= 6 {
                    self.pageVC.topNews = [NewsModel](news.suffix(from: 6))
                }
            }
            
        }) { error in
            SVProgressHUD.dismiss()
            self.showAlert(error.localizedDescription)
        }
    }
    func showAlert(_ message: String) {
        let alert = UIAlertController(title: "Warning!", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func createItemLayout() {
        let margin : CGFloat = 0
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsetsMake(margin, margin, margin, margin)
        layout.itemSize = CGSize(width: widthScreen, height: heightCell)
        
        layout.minimumLineSpacing = 15
        
        collectionView?.collectionViewLayout = layout
    }
    
    // MARK: UICollectionViewDataSource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return news.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let pieceOfNews = news[indexPath.row]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ImageCollectionViewCell
        
        cell.labelTitle.text = pieceOfNews.title
        cell.labelURL.text = pieceOfNews.domainName
        cell.labelTime.text = pieceOfNews.date
        
        if let url = pieceOfNews.coverURL {
            cell.imageView.kf.setImage(with: url)
        }
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let link = news[indexPath.item].link,
            let url = URL(string: link) else { return }
        UIApplication.shared.open(url, options: [ : ], completionHandler: nil)
    }
    
    func addTopNews() {
        collectionView?.contentInset = UIEdgeInsets(top: 325, left: 0, bottom: 0, right: 0)
        pageVC.view.frame = CGRect(x: 0, y: -325, width: widthScreen, height: heightCell)
        collectionView?.addSubview(pageVC.view)
        addChildViewController(pageVC)
    }
}

extension NewsCollectionViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if news[indexPath.row].coverURL == nil {
            return CGSize(width: widthScreen, height: heightCell - 206)
        }
        return CGSize(width: widthScreen, height: heightCell)
    }
}
