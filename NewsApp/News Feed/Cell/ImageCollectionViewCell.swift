//
//  ImageCollectionViewCell.swift
//  
//
//  Created by Anna on 30.09.17.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelURL: UILabel!
    @IBOutlet weak var labelTime: UILabel!
}
