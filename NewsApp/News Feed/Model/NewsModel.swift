//
//  NewsModel.swift
//  NewsApp
//
//  Created by Anna on 30.09.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import Unbox
import UIKit

struct NewsModel {
    var id: Int
    var title: String
    var overview: String?
    var voteCount: Int
    var voteAverage: Int
    var releaseDate: String
    var backdropPath: String?
    var posterPath: String?
    
    var link: String?
    var isTopNews: Bool?
    
    var coverURL: URL? {
        guard let path = backdropPath else { return nil }
        let urlStr = "https://image.tmdb.org/t/p/w500" + path
        return URL(string: urlStr)
    }
    
    var date: String {
        let dateFormatter = DateFormatter.dateFromServer
        guard let date = dateFormatter.date(from: releaseDate) else {return ""}
        return date.getElapsedInterval()
    }
    
    var domainName: String {
        guard let link = link,
            let url = URL(string: link),
            let hostString = url.host else { return "api.themoviedb.org" }
        
        if hostString.hasPrefix("www.") {
            return String(hostString.dropFirst(4))
        }
        return hostString
    }
    
}

extension NewsModel: Unboxable {
    init(unboxer: Unboxer) throws {
        self.id = try unboxer.unbox(key: "id")
        self.title = try unboxer.unbox(key: "title")
        self.overview = try? unboxer.unbox(key: "overview")
        self.voteCount = try unboxer.unbox(key: "vote_count")
        self.voteAverage = try unboxer.unbox(key: "vote_average")
        self.releaseDate = try unboxer.unbox(key: "release_date")
        self.posterPath = try? unboxer.unbox(key: "poster_path")
        self.backdropPath = try? unboxer.unbox(key: "backdrop_path")
    }
}
