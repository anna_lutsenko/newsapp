//
//  DateFormatter+Ext.swift
//  NewsApp
//
//  Created by Anna on 01.10.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import Foundation
extension DateFormatter {
    
    static var dateFromServer : DateFormatter {
//        "2017-09-26T15:22:09.947Z" -> "yyyy-MM-dd'T'HH:mm:ss.SSZ"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter
    }
}
