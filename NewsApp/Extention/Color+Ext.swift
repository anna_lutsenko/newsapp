//
//  Color+Ext.swift
//  NewsApp
//
//  Created by Anna on 01.10.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    convenience init(r: Int, g: Int, b: Int, a: Int = 255) {
        let red = CGFloat(r)/255
        let green = CGFloat(g)/255
        let blue = CGFloat(b)/255
        let alpha = CGFloat(a)/255
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}
