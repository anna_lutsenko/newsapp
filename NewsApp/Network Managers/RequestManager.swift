//
//  RequestManager.swift
//  NewsApp
//
//  Created by Anna on 30.09.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import Foundation
import Alamofire
import Unbox

struct URLConstants {
    static let newsURL = "https://api.themoviedb.org/3/movie/popular?api_key=f910e2224b142497cc05444043cc8aa4&language=en-US&page=1"
}

enum RequestError : Error {
    case parsingError
    case unknownError
}

class RequestManager {
    static let sharedInstance = RequestManager()
    
    func getData(success: @escaping ([NewsModel])->(), failure: @escaping (Error)->()) {
        Alamofire.request(URLConstants.newsURL)
        
            .responseJSON { response in
                
                switch response.result {
                case .success(let json):
                    
                    
                    guard let jsonDict = json as? [String: Any],
                        let unboxableDictionary =  jsonDict["results"] as? [UnboxableDictionary] else {
                        failure(RequestError.parsingError)
                        return
                    }
                    do {
                        let newsArr: [NewsModel] = try unbox(dictionaries: unboxableDictionary)
                        success(newsArr)
                    } catch let error {
                        failure(error)
                    }
                    
                case .failure(let error):
                    failure(error)
                }
        }
    }
    
}
